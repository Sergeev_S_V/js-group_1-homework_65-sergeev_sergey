import React from 'react';
import NavigationItem from "./NavigationItem/NavigationItem";

const Navigation = () => (
  <nav className="navbar navbar-default">
    <div className="container-fluid">
      <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul className="nav navbar-nav">
          <NavigationItem to='/'>Home</NavigationItem>
          <NavigationItem to='/pages/about'>About us</NavigationItem>
          <NavigationItem to='/pages/cooperation'>Cooperation</NavigationItem>
          <NavigationItem to='/pages/press-center'>Press-center</NavigationItem>
          <NavigationItem to='/pages/career'>Career</NavigationItem>
          <NavigationItem to='/pages/contact-info'>Contact info</NavigationItem>
          <NavigationItem to='/pages/admin'>Admin</NavigationItem>
        </ul>
      </div>
    </div>
  </nav>
);

export default Navigation;