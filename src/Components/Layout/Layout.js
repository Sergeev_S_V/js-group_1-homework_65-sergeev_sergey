import React, {Fragment} from 'react';
import './Layout.css';
import Header from "../Header/Header";
import Footer from "../Footer/Footer";

const Layout = props => (
  <Fragment>
    <Header/>
    <main>{props.children}</main>
    <Footer/>
  </Fragment>
);

export default Layout;