import React from 'react';
import './Footer.css';

const Footer = () => (
  <footer className="flex navbar navbar-default">
    <h3>Footer</h3>
    <div>
      <p>Footer item</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, numquam?</p>
    </div>
    <div>
      <p>Footer item</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, numquam?</p>
    </div>
    <div>
      <p>Footer item</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, numquam?</p>
    </div>
  </footer>
);

export default Footer;