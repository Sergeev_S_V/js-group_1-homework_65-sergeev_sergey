import React, {Component} from 'react';
import axios from 'axios';
import './Content.css';
import Spinner from "../../Components/UI/Spinner/Spinner";

class Content extends Component {

  state = {
    currentContent: {},
    loading: true,
  };

  getContent = () => {
    axios.get(`/${this.props.match.params.content}.json`)
      .then(response => {
        response.data
          ? this.setState({currentContent: response.data, loading: false})
          : null;
      })
  };

  componentDidUpdate(prevProps) {
    this.props.match.params.content !== prevProps.match.params.content
      ? this.getContent()
      : null;
  };

  componentDidMount() {
    this.getContent();
  };

  render() {
    let content;
    if (this.state.loading) {
      content = <Spinner/>
    } else {
      content = (
        <div className='Content-Block'>
          <h3 className='Title'>{this.state.currentContent.title}</h3>
          <div className='Content' dangerouslySetInnerHTML={{__html: this.state.currentContent.content}}/>
          <div className='Content' dangerouslySetInnerHTML={{__html: this.state.currentContent.content}}/>
        </div>
      )
    }

    return content
  }
}

export default Content;