import React, { Component } from 'react';
import './App.css';
import Layout from "./Components/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Content from "./Containers/Content/Content";
import WelcomePage from "./Components/WelcomePage/WelcomePage";
import EditContent from "./Containers/EditContent/EditContent";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path='/' exact component={WelcomePage}/>
          <Route path='/pages/admin' exact component={EditContent}/>
          <Route path='/pages/:content' component={Content}/>
        </Switch>
      </Layout>
    );
  }
}

export default App;
